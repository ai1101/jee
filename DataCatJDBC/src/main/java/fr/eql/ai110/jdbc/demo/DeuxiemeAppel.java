package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

public class DeuxiemeAppel {

	public static void main(String[] args) {
		
		DataSource ds = new CatDataSource();
		Connection cnx = null;
		
		try {
			cnx = ds.getConnection();
			System.out.println("connexion ok");
			
			//Requête SQL :
			String req = "INSERT into CAT (name, race, birth) VALUES ('Duchesse', 'Persan', '2002-12-03')";
			Statement stmt = cnx.createStatement();
			int success = stmt.executeUpdate(req);
			
			//méthode 1 : 
			System.out.println(success == 1 ? "insert ok" : "ça marche paaas");
			
			//méthode 2 : équivalente à méthode 1
			if (success == 1) {
				System.out.println("insert ok");
			} else {
				System.out.println("ça marche paaas");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
				System.out.println("connexion fermée");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
