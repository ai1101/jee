package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PremierAppel {

	public static void main(String[] args) {
		
		//Utilisateur de la BDD
		String username = "root";
		//password de la BDD
		String password = "root";
		//URL de notre BDD avec quelques paramètres :
		String url = "jdbc:mysql://127.0.0.1:3306/cat_db?"
				+ "serverTimezone=Europe/Paris"
				+ "&verifyServerCertificate=false"
				+ "&useSSL=false"
				+ "&allowPublicKeyRetrieval=true";
		//Driver
		String driver = "com.mysql.cj.jdbc.Driver";
		
		Connection cnx = null;
		
		try {
			//Charge en mémoire le Driver :
			Class.forName(driver);
			//Connexion à la BDD :
			cnx = DriverManager.getConnection(url, username, password);
			System.out.println("Connection ok");
			
			//Requête SQL :
			String req = "INSERT into CAT (name, race, birth) VALUES ('Robert', 'norvégien','2016-04-29')";
			//Le statement est un objet qui encapsule la requête SQL :
			Statement stmt = cnx.createStatement();
			//On execute la requête
			int success = stmt.executeUpdate(req);
			//On vérifie le bon fonctionnement de la transaction grâce à une condition ternaire
			System.out.println( success == 1 ? "insert ok" : "ça marche pas");
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			//On ferme la connexion à la BDD :
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		

	}

}
