package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import javax.sql.DataSource;

public class DemoSelectWithParameters {

	public static void main(String[] args) {

		DataSource ds = new CatDataSource();
		Connection cnx = null;
		
		//Requête spéciale : on veut récupérer tous les chats dont le nom contient "ch" : 
		String expectedPartialName = "ch";
		String req = "SELECT id, name, race, birth FROM cat WHERE name LIKE ?";
		
		try {
			cnx = ds.getConnection();
			//On crée un preparedstatement pour encapsuler la requete paramétrée :
			PreparedStatement pstmt = cnx.prepareStatement(req);
			
			//On set le paramètre de la requête :
			pstmt.setString(1, "%" + expectedPartialName + "%");
			
			//On éxecute la requete et on récupère le résultat dans un ResultSet: 
			ResultSet rs = pstmt.executeQuery();
			
			//on itère sur le resultset :
			while(rs.next()) {
				//pour chaque ligne, on récupère nos champs : 
				Integer id = rs.getInt(1);
				String name = rs.getString("name");
				String race = rs.getString("race");
				LocalDate birth = rs.getDate("birth").toLocalDate();
				
				//j'affiche les infos de chaque chat en console :
				StringBuilder sb = new StringBuilder();
				sb.append("id :").append(id.toString())
				.append("\tname : ").append(name)
				.append("\trace : ").append(race)
				.append("\tbirth : ").append(birth.toString());
				
				System.out.println(sb);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
