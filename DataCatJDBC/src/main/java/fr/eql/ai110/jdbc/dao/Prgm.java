package fr.eql.ai110.jdbc.dao;

import java.util.List;

public class Prgm {

	public static void main(String[] args) {

		IDaoCat dao = new DaoCat();

		//Test findById : 
		Cat azrael = dao.findById(2);
		System.out.println(azrael.toString());

		//Test addCat : 
		/*
		Cat felix = new Cat();
		felix.setName("felix");
		felix.setRace("chat blanc");
		felix.setBirth(LocalDate.now());
		dao.addCat(felix);
		 */

		//Test findByName :
		List<Cat> cats1 = dao.findByName("felix");
		for (Cat cat : cats1) {
			System.out.println(cat.toString());
		}

		//test de la méthode getAll() :
		System.out.println("*********");
		List<Cat> cats = dao.getAll();
		System.out.println(cats.size());

		//test de la méthode delete : 
		//dao.delete(azrael);

	}

}
