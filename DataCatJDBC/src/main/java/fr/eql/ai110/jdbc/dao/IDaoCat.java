package fr.eql.ai110.jdbc.dao;

import java.util.List;

public interface IDaoCat {

	List<Cat> getAll();
	Cat findById(Integer id);
	List<Cat> findByName(String name);
	void addCat(Cat cat);
	void delete(Cat cat);
	List<Cat> findByUser(User user);
}
