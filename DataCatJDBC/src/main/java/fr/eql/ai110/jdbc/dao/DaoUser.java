package fr.eql.ai110.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import fr.eql.ai110.jdbc.demo.CatDataSource;

public class DaoUser implements IDaoUser {
	
	private DataSource ds = new CatDataSource();
	private static final String REQ_AUTH = "SELECT * FROM user WHERE login = ? AND password = ?";

	@Override
	public User authenticate(String login, String password) {
		Connection cnx = null;
		User user = null;
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_AUTH);
			pstmt.setString(1, login);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				user = new User(rs.getInt("id"), rs.getString("login"), rs.getString("password"), 
						rs.getString("firstName"), rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
		
	}

}
