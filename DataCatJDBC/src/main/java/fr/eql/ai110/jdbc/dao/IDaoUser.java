package fr.eql.ai110.jdbc.dao;

public interface IDaoUser {
	
	User authenticate(String login, String password);

}
