package fr.eql.ai110.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import fr.eql.ai110.jdbc.demo.CatDataSource;

public class DaoCat implements IDaoCat {
	
	private DataSource ds = new CatDataSource();
	private static final String REQ_GET = "SELECT * FROM cat";
	private static final String REQ_ADD = "INSERT INTO cat (name, race, birth, photo) VALUES (?, ?, ?, ?)";
	private static final String REQ_FIND_BY_ID = "SELECT * FROM cat where id like ?";
	private static final String REQ_FIND_BY_NAME = "SELECT * FROM cat WHERE name like ?";
	private static final String REQ_DELETE = "DELETE FROM cat where cat.id = ?";
	private static final String REQ_FIND_BY_USER = "SELECT * FROM cat WHERE user_id = ?";
	
	@Override
	public List<Cat> getAll() {
		Connection cnx = null;
		List<Cat> cats = new ArrayList<Cat>();
		
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_GET);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Cat cat = new Cat(rs.getInt("id"), 
						rs.getString("name"),
						rs.getString("race"),
						rs.getDate("birth").toLocalDate(),
						rs.getString("photo"));
				cats.add(cat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cats;
	}

	@Override
	public Cat findById(Integer id) {
		Cat cat = new Cat();
		Connection cnx = null;
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_FIND_BY_ID);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				cat.setId(rs.getInt("id"));
				cat.setName(rs.getString("name"));
				cat.setRace(rs.getString("race"));
				cat.setBirth(rs.getDate("birth").toLocalDate());
				cat.setPhoto(rs.getString("photo"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return cat;
	}

	@Override
	public List<Cat> findByName(String name) {
		List<Cat> cats = new ArrayList<Cat>();
		Connection cnx = null;
		
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_FIND_BY_NAME);
			StringBuilder sb = new StringBuilder();
			sb.append("%").append(name).append("%");
			pstmt.setString(1, sb.toString());
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Cat cat = new Cat(rs.getInt("id"), 
						rs.getString("name"),
						rs.getString("race"), 
						rs.getDate("birth").toLocalDate(),
						rs.getString("photo"));
				cats.add(cat);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cats;
	}

	@Override
	public void addCat(Cat cat) {
		Connection cnx = null;
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_ADD);
			pstmt.setString(1, cat.getName());
			pstmt.setString(2, cat.getRace());
			pstmt.setDate(3, Date.valueOf(cat.getBirth()));
			pstmt.setString(4, cat.getPhoto());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Cat cat) {
		Connection cnx = null;
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_DELETE);
			pstmt.setInt(1, cat.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Cat> findByUser(User user) {
		Connection cnx = null;
		List<Cat> cats = new ArrayList<Cat>();
		try {
			cnx = ds.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(REQ_FIND_BY_USER);
			pstmt.setInt(1, user.getId());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				cats.add(new Cat(rs.getInt("id"), rs.getString("name"), rs.getString("race"), rs.getDate("birth").toLocalDate(), rs.getString("photo")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cats;
	}

}
