package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.sql.DataSource;

public class DemoPreparedStatement {

	public static void main(String[] args) {

		DataSource ds = new CatDataSource();
		Connection cnx = null;
		
		//On récupère de l'utilisateur les infos d'un chat à persister en BDD :
		String name = "Pattenrond";
		String race = "Chat roux";
		String birth = "05/10/1998";
		
		try {
			cnx = ds.getConnection();
			//Ne valide pas automatiquement la transaction après chaque requête :
			cnx.setAutoCommit(false);
			
			String req = "INSERT into cat (name, race, birth) VALUES (?,?,?)";
			
			PreparedStatement pstmt = cnx.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, name);
			pstmt.setString(2, race);
			
			//Recette pour obtenir une date SQL à partir d'une chaine de caractères:
			//ex format initial : dd/MM/yyyy
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate date = LocalDate.parse(birth, formatter);
			Date sqlBirth = Date.valueOf(date);
			
			pstmt.setDate(3, sqlBirth);
			
			int success = pstmt.executeUpdate();
			
			System.out.println(success == 1 ? "insert ok" : "ça marche pas");
			
			//Si tout se passe bien, on valide la transaction :
			cnx.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			//S'il y a eu un problème, on rollback :
			try {
				cnx.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
