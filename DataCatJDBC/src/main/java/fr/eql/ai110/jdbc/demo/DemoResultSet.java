package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import javax.sql.DataSource;

public class DemoResultSet {

	public static void main(String[] args) {
		
		DataSource ds = new CatDataSource();
		Connection cnx = null;
		String req = "SELECT id, name, race, birth from CAT";
		
		try {
			cnx = ds.getConnection();
			Statement stmt = cnx.createStatement();
			
			//On execute la requete et on stocke le résultat dans un ResultSet :
			ResultSet rs = stmt.executeQuery(req);
			
			//On itère sur le résultat, tant qu'il reste des lignes :
			while (rs.next()) {
				// Pour chaque ligne du ResultSet, on récupère nos champs :
				Integer id = rs.getInt(1);
				String name = rs.getString("name");
				String race = rs.getString("race");
				LocalDate birth = rs.getDate("birth").toLocalDate();
				
				//affichage du chat en console :
				StringBuilder sb = new StringBuilder();
				sb.append("id : ").append(id.toString())
				.append("\tname :").append(name)
				.append("\trace : ").append(race)
				.append("\tbirth :").append(birth.toString());
				
				System.out.println(sb);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
