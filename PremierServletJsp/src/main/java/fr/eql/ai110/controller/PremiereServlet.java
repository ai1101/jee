package fr.eql.ai110.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Une servlet hérite de HttpServlet

public class PremiereServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	//Une servlet doit avoir un constructeur vide explicite :
	public PremiereServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		//objet qui permet d'envoyer des strings au client :
		PrintWriter out = resp.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Ma première servlet</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1> Coucou les AI 110 !!!! </h1>");
		
		out.println("<form method='POST' action='SecondeServlet'>"); 
		out.println("Login : <input type='text' name='login'/> <br/>");
		out.println("Password : <input type='password' name='password'/> <br/>");
		out.println("Genre : <input type='radio' name='genre' value='F'/> Femme ");
		out.println("<input type='radio' name='genre' value='H'/> Homme ");
		out.println("<input type='radio' name='genre' value='A'/> Autre <br/>");
		out.println("Pays : <select name='pays'>");
		out.println("<option value='--'> Choisir ...</option>");
		out.println("<option value='France'>France</option>");
		out.println("<option value='Egypte'>Egypte</option>");
		out.println("<option value='Burkina Faso'>Burkina Faso</option>");
		out.println("</select><br/>");
		out.println("<input type='checkbox' name='optin'/>J'accepte les conditions d'utilisation <br/>");
		out.println("<input type='submit' value='valider'/>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
