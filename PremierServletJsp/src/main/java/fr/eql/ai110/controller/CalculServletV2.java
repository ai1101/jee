package fr.eql.ai110.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calculV2")
public class CalculServletV2 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public CalculServletV2() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String op = req.getParameter("op");
		String nb1 = req.getParameter("n1");
		String nb2 = req.getParameter("n2");
		String operation = "";
		
		//variables :
		int num1 = 0;
		int num2 = 0;
		int num3 = 0;
		
		//Conversion :
		num1 = Integer.parseInt(nb1);
		num2 = Integer.parseInt(nb2);
		
		//calcul :
		switch(op) {
		case "add":
			num3 = num1 + num2;
			operation = "Addition";
			break;
		case "sub":
			num3 = num1 - num2;
			operation = "Soustraction";
			break;
		case "mul":
			num3 = num1 * num2;
			operation = "Multiplication";
			break;
		case "div":
			num3 = num1 / num2;
			operation = "Division";
			break;
		}
		
		//Stocker les résultats dans la requête : 
		req.setAttribute("operation", operation);
		req.setAttribute("result", num3);
		
		//Rediriger vers une page jsp qui affichera le résultat : 
		RequestDispatcher disp = req.getRequestDispatcher("resultV2.jsp");
		disp.forward(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
	
}
