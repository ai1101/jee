package fr.eql.ai110.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eql.ai110.jdbc.dao.Cat;
import fr.eql.ai110.jdbc.dao.DaoCat;
import fr.eql.ai110.jdbc.dao.IDaoCat;

@WebServlet("/displayCatServlet")
public class DisplayCatServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public DisplayCatServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		//Je fais appel au daoCat de DataCatJDBC
		IDaoCat dao = new DaoCat();
		//On récupère une liste de tous les chats de la BDD via la méthode getAll()
		List<Cat> cats = dao.getAll();
		
		//stocke la liste de chats dans req, et on renvoie vers la vue:
		req.setAttribute("cats", cats);
		req.getRequestDispatcher("displayCats.jsp").forward(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
