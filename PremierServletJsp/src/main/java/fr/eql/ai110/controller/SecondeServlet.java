package fr.eql.ai110.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SecondeServlet") //2eme manière pour déclarer une servlet
public class SecondeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public SecondeServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Ma Seconde Servlet</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1> Vos informations : </h1>");
		out.println("<ul>");
		out.println("<li> Login : " + req.getParameter("login") + "</li>");
		out.println("<li> Password : " + req.getParameter("password") + "</li>");
		out.println("<li> Genre : " + req.getParameter("genre") + "</li>");
		out.println("<li> Pays : " + req.getParameter("pays") + "</li>");
		out.println("<li> Optin : " + req.getParameter("optin") + "</li>");
		out.println("</ul>");
		
		//On redirige vers la troisième
		out.println("<a href='ThirdServlet'> Page suivante </a>");
		
		out.println("</body>");
		out.println("</html>");
		
		HttpSession session = req.getSession();
		session.setAttribute("leLogin", req.getParameter("login"));
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
