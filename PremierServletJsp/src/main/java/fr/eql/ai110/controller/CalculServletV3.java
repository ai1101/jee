package fr.eql.ai110.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eql.ai110.model.CalculService;
import fr.eql.ai110.model.Result;

@WebServlet("/calculV3")
public class CalculServletV3 extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public CalculServletV3() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		//Récupération des params :
		String op = req.getParameter("op");
		String nb1 = req.getParameter("n1");
		String nb2 = req.getParameter("n2");
		
		//Convertir en entier :
		int num1 = Integer.parseInt(nb1);
		int num2 = Integer.parseInt(nb2);
		
		//Traitement par appel à notre service CalculService :
		CalculService service = new CalculService();
		Result result = service.calcul(op, num1, num2);
		
		//stocker result dans la requête : 
		req.setAttribute("result", result);
		
		//Redirection vers une JSP : 
		RequestDispatcher disp = req.getRequestDispatcher("resultV3.jsp");
		disp.forward(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
