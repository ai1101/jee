package fr.eql.ai110.model;

public class Result {

	private String operation;
	private int num;
	
	public Result() {
		super();
	}
	public Result(String operation, int num) {
		super();
		this.operation = operation;
		this.num = num;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
	
}
