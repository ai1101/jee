package fr.eql.ai110.model;

public class CalculService {

	public Result calcul(String op, int num1, int num2) {
		Result res = new Result();
		
		switch (op) {
		case "add":
			res.setNum(num1 + num2);
			res.setOperation("Addition");
			break;
		case "sub":
			res.setNum(num1 - num2);
			res.setOperation("Soustraction");
			break;
		case "mul":
			res.setNum(num1 * num2);
			res.setOperation("Multiplication");
			break;
		case "div":
			res.setNum(num1 / num2);
			res.setOperation("Division");
			break;
		}
		return res;
	}
}
