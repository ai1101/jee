<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="result" class="fr.eql.ai110.model.Result" scope="request"/>

<html>
<head>
<meta charset="UTF-8">
<title>Result V3</title>
</head>
<body>

	Vous avez choisi : <jsp:getProperty property="operation" name="result"/>
	<br/>
	Le résultat est <jsp:getProperty property="num" name="result" />	

</body>
</html>