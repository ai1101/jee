<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Deconnection page</title>
</head>
<body>
	Bye bye ${user.login} ! 
	<a href="DeconnectionServlet"> Retour à la page de connexion </a>
</body>
</html>