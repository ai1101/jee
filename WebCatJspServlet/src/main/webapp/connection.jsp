<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Connection page</title>
</head>
<body>
	<h1> Renseigner login et mot de passe pour vous connecter :</h1>
	<form method="post" action="ConnectionServlet">
		Login : <input type="text" name="login"/>
		<br/>
		Mot de passe : <input type="password" name="password"/>
		<br/>
		<input type="submit" value="Se Connecter"/>  
	</form>
	<c:if test="${message!=null}">
		<label style="color:red"> ${message}</label>
	</c:if>
</body>
</html>