package fr.eql.ai110.webcat.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/DeconnectionServlet")
public class DeconnectionServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;

	public DeconnectionServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		HttpSession session = req.getSession();
		//pour retirer l'utilisateur qui était en session, j'utilise la méthode invalidate()
		session.invalidate();
		//on redirige vers la page de connection
		req.getRequestDispatcher("connection.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
