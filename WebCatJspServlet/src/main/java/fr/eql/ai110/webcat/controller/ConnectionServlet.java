package fr.eql.ai110.webcat.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.jdbc.dao.Cat;
import fr.eql.ai110.jdbc.dao.DaoCat;
import fr.eql.ai110.jdbc.dao.DaoUser;
import fr.eql.ai110.jdbc.dao.IDaoCat;
import fr.eql.ai110.jdbc.dao.IDaoUser;
import fr.eql.ai110.jdbc.dao.User;

@WebServlet("/ConnectionServlet")
public class ConnectionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ConnectionServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		IDaoUser daoUser = new DaoUser();
		String message = null;
		//recupère les champs remplis par l'utilisateur :
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		//récupère le user authentifié ou null si échec
		User user = daoUser.authenticate(login, password);
		
		if (user != null) {
			//met l'user en session
			HttpSession session = req.getSession();
			session.setAttribute("user", user);
			
			//récupère ses chats
			IDaoCat daoCat = new DaoCat();
			List<Cat> cats = daoCat.findByUser(user);
			req.setAttribute("cats", cats);
			
			//forward vers la page d'affichage de ses chats :
			req.getRequestDispatcher("displayUsersCat.jsp").forward(req, resp);
		} else {
			//si user == null, implémente un message pour lui indiquer login ou password incorrect
			message = "login ou password incorrect";
			//envoie le message à la vue
			req.setAttribute("message", message);
			//redirige sur la page de connection pour qu'il retente
			req.getRequestDispatcher("connection.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		doGet(req, resp);
	}

	
}
